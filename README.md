Tutorial Microsserviços - Projeto calculadora
===
---
Primeiros Passos
---
- Clone esse repositório para o seu computador usando o git
- Certifique-se que você possui uma conta no hub.docker.com
- Siga as instruções do workshop para configurar o seu microsserviço
    - Atenção! Você deverá escolher qual microsserviço deseja fazer.

Opções de microsserviço
---
- Serviço soma
- Serviço subtração
- Serviço multiplicação
- Serviço divisão
- Serviço Api Gateway
    - Cuidado! Esse é o serviço mais complexo de se desenvolver!


Dockerfile
---
Esse arquivo tem como objetivo descrever os passos da construção de uma imagem. Ele suporta várias instruções, dentre elas as mais comuns são:
- FROM
- EXPOSE
- WORKDIR
- RUN
- COPY
- ADD
- ENTRYPOINT
- CMD
### FROM
Essa instrução indica qual repositório do https://hub.docker.com/ você utilizará como imagem base.
~~~docker
FROM python:3.7-buster
~~~
### EXPOSE
É a instrução que determina quais portas deverão ser exportadas do container.
~~~docker
EXPOSE 5000
~~~
### WORKDIR
Descreve a pasta que você utilizará como raiz do seu projeto.
~~~docker
WORKDIR /app
~~~
### RUN
É a instrução mais poderosa do docker, pois é responsável por executar um comando shell script.
~~~docker
RUN apt-get update && apt-get install sudo -y && \
    rm -rf /var/lib/apt/lists/* && \
    adduser --disabled-password --gecos '' --uid 1001 docker && \
    gpasswd -a docker sudo && \
    echo '%sudo ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers && \
    chown -R docker:docker /app

.
.
.

RUN sudo pip install -r requirements.txt
~~~
#### Perceba que agrupamos os comandos utilizando &&, pois a cada instrução utilizada em seu Dockerfile uma "imagem-intermediária" será gerada. Assim tornamos o processo mais eficiente

### COPY
Transfere arquivos da sua máquina real para a imagem.
~~~docker
COPY --chown=docker . /app
~~~

#### ADD é um comando similar, mas que possui duas diferenças importantes veja [aqui](https://stackoverflow.com/questions/24958140/what-is-the-difference-between-the-copy-and-add-commands-in-a-dockerfile)

### CMD
Indica o comando que deve ser executado na inicialização do container
~~~docker
CMD ["python","run.py"]
~~~
#### Entrypoint se assemelha ao CMD, mas não pode ser sobrescrito pelo docker-compose.yml

## docker-compose.yml
~~~docker
version: "3.5"
services:
        NOME_SERVICO:
                image: NOME_CONTA_DOCKER_HUB/NOME_REPOSITORIO
                build: # build local Dockerfile
                        context: .
                        dockerfile: Dockerfile
                restart: on-failure # restart if this container fails
                deploy: # on swarm mode...
                        replicas: 3 # starts with 6 replicas of this container
                        restart_policy:
                                condition: on-failure # restart if this container fails
                                delay: 5s # wait 5 seconds until restarting process
                        update_config: # on update...
                                parallelism: 2 # updates two replicas at a time
                                failure_action: rollback # if deploy fails, rollback
                                monitor: 1s # wait 1 second for deploy failures
                                order: start-first # start the new version b4 closing the old one
                networks:
                        - services_net
networks:
        services_net:
                external: true

~~~
## .gitlab-ci.yml
É o arquivo de configuração do pipeline DevOps do Gitlab.
~~~
image: tmaier/docker-compose:latest

services:
  - docker:dind

stages:
        - build
        - test
        - deploy
before_script:
        - whoami
        - docker info
        - docker-compose --version
        - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
buildCompose:
        stage: build
        script:
                - echo "Building..."
                - docker-compose build
                - docker-compose push

runTestScript:
        stage: test
        script:
                - echo "Testing..."
                - docker network create services_net
                - docker-compose run user pytest -v --disable-warnings

deployToCloud:
        stage: deploy
        script:
                - echo "Deploying..."
                - apk add --no-cache sshpass rsync openssh-client
                - sshpass -p "$PROD_SSH_PASSWD" rsync -avzh -e 'ssh -o StrictHostKeyChecking=no -p 22' ./docker-compose.yml $PROD_SSH_USER_AND_IP:~/docker-compose.yml
                - sshpass -p "$PROD_SSH_PASSWD" ssh -o StrictHostKeyChecking=no $PROD_SSH_USER@$PROD_SSH_IP "sudo docker pull mymobiconf/userservice && sudo docker pull mongo:4-bionic"
                - sshpass -p "$PROD_SSH_PASSWD" ssh -o StrictHostKeyChecking=no $PROD_SSH_USER@$PROD_SSH_IP "sudo docker stack deploy -c docker-compose.yml userservice"

~~~

## run.py
- Defina um namespace
~~~python
ns = Namespace(name="", description="", path="")
api.add_namespace(ns)
~~~

- Crie uma rota
~~~python
@ns.route("/")
class Addition(Resource):
    def get(self):
        return ""
~~~
- Você precisará utilizar flask.request.args [veja a referência de flask aqui](https://flask.palletsprojects.com/en/1.1.x/reqcontext/)